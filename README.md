# djangotools
Contains some shellscripts help develop django-based frameworks much faster!
## Installation
:memo: call these shellscripts anywhere you want!

:heavy_check_mark: my recommendation is to setup a ~/bin for your shell!

:heavy_exclamation_mark: be aware to activate yout venv when you are using these scripts
## Index
* ##### startapp:
  like `python manage.py startapp APP_NAME` creates an app but with structure below:
  ```bash
  PROJECT_NAME/
  manage.py
  APP_NAME/
    models/
        __init__.py
    serializers/
        __init__.py
    views/
        __init__.py
    urls/
        __init__.py
    tests/
        __init__.py
  ```
  as you can see, there's no `models.py`, `admin.py`, `tests.py`, or even `views.py`
  ###### Usage:
  ```bash
  startapp APP_NAME
  ```
* ##### addmodel
  adding a model will change mentioned structure like this:
  ```bash
  PROJECT_NAME/
  manage.py
  APP_NAME/
    models/
        __init__.py
        MODEL_NAME.py
    serializers/
        __init__.py
        MODEL_NAME/
            create.py
            list.py
            update.py
            retrieve.py
    views/
        __init__.py
        MODEL_NAME.py
    urls/
        __init__.py
        MODEL_NAME.py
    tests/
        __init__.py
  ```
  ###### Usage:
  ```bash
  addmodel APP_NAME MODEL_NAME MODEL_CLASS_NAME
  ```
  `MODEL_CLASS_NAME` is a camel/Pascal/... case name that you want to name your python class model.