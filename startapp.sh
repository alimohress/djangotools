#!/bin/bash

APP_NAME=$1
MODELS=models
SERIALIZERS=serializers
VIEWS=views
TESTS=tests
URLS=urls
# creating basic app structure using manage.py django
python manage.py startapp $APP_NAME

# remove nasty files 
rm $APP_NAME/models.py \
    $APP_NAME/tests.py \
    $APP_NAME/admin.py \
    $APP_NAME/views.py

# make models, serializers, views, tests & urls as a package
mkdir -p $APP_NAME/$MODELS\
        $APP_NAME/$SERIALIZERS \
        $APP_NAME/$TESTS \
        $APP_NAME/$VIEWS \
        $APP_NAME/$URLS 

touch $APP_NAME/$MODELS/__init__.py \
        $APP_NAME/$SERIALIZERS/__init__.py \
        $APP_NAME/$TESTS/__init__.py \
        $APP_NAME/$VIEWS/__init__.py
        
echo -e "urlpatterns=[]\n" > $APP_NAME/$URLS/__init__.py