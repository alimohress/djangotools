#!/bin/bash

APP_NAME=$1
MODEL_NAME=$2
MODEL_CLASS_NAME=$3


function make_model() {

        touch $APP_NAME/models/$MODEL_NAME.py
        echo -e "from django.db import models\n\n\nclass ${MODEL_CLASS_NAME}:\n\tpass" > $APP_NAME/models/$MODEL_NAME.py
        echo -e "from .${MODEL_NAME} import ${MODEL_CLASS_NAME}" >> $APP_NAME/models/__init__.py
}

function make_serializers() {

        ACTIONS=(
                "create"
                "list"
                "retrieve"
                "update"
        )
        SERIALIZERS_NAMES=(
                "${MODEL_CLASS_NAME}${ACTIONS[0]^}Serializer"
                "${MODEL_CLASS_NAME}${ACTIONS[1]^}Serializer"
                "${MODEL_CLASS_NAME}${ACTIONS[2]^}Serializer" 
                "${MODEL_CLASS_NAME}${ACTIONS[3]^}Serializer"
        )
        mkdir -p $APP_NAME/serializers/$MODEL_NAME/
        touch $APP_NAME/serializers/$MODEL_NAME/__init__.py
        for ((i = 0 ; i < ${#ACTIONS[@]} ; i++)); do
                echo -e "from rest_framework import serializers\nclass ${SERIALIZERS_NAMES[$i]}:\n\tpass" > $APP_NAME/serializers/$MODEL_NAME/"${ACTIONS[$i]}.py"
                echo -e "from .${ACTIONS[$i]} import ${SERIALIZERS_NAMES[$i]}" >> $APP_NAME/serializers/$MODEL_NAME/__init__.py
        done
        echo -e "from .${MODEL_NAME} import *" >> $APP_NAME/serializers/__init__.py

}

function make_views() {
        VIEWSET_NAME="${MODEL_CLASS_NAME}ViewSet"
        echo -e "from rest_framework import viewsets\nclass ${VIEWSET_NAME}:\n\tpass" > $APP_NAME/views/$MODEL_NAME.py
        echo -e "from .${MODEL_NAME} import ${VIEWSET_NAME}" >> $APP_NAME/views/__init__.py
}

function make_urls() {
        VIEWSET_NAME="${MODEL_CLASS_NAME}ViewSet"
        echo -e "from rest_framework.routers import DefaultRouter\n" > $APP_NAME/urls/$MODEL_NAME.py
        echo -e "from ..views import ${VIEWSET_NAME}\n" >> $APP_NAME/urls/$MODEL_NAME.py
        echo -e "router = DefaultRouter()\n" >> $APP_NAME/urls/$MODEL_NAME.py
        echo -e "router.register(r'${MODEL_NAME}s',${VIEWSET_NAME})\n" >> $APP_NAME/urls/$MODEL_NAME.py
        echo -e "urlpatterns = router.urls\n" >> $APP_NAME/urls/$MODEL_NAME.py

        echo -e "from .${MODEL_NAME} import urlpatterns as ${MODEL_NAME}_urls\n" >> $APP_NAME/urls/__init__.py
        echo -e "urlpatterns+=${MODEL_NAME}_urls\n" >> $APP_NAME/urls/__init__.py
}

make_model
make_serializers
make_views
make_urls
